<?php

$lang['resource_report_app_description'] = 'Додаток «Звіт про ресурси» містить інформацію про навантаження, використання пам’яті та запущені процеси.';
$lang['resource_report_app_name'] = 'Звіт про ресурси';
$lang['resource_report_load_average'] = 'Середнє навантаження';
$lang['resource_report_number_of_processes'] = 'Кількість процесів';
